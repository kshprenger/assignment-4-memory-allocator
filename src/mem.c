#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 ); 
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  block_size size_from_query_capacity = size_from_capacity((block_capacity){.bytes=query});          // В запросе не учтена память на header
  size_t actual = region_actual_size(size_from_query_capacity.bytes);                                // Ближайшее округление до правильного(кратного размеру страницы) размера региона
  void* alloc_region_addr = map_pages(addr,actual,MAP_FIXED_NOREPLACE);                              // Через MAP_FIXED_NOREPLACE мы не сможем занять уже размеченную память
  if(alloc_region_addr==MAP_FAILED){
    alloc_region_addr = map_pages(addr,actual,0);                                                    // По указанному адресу не получилось, значит попробуем где-нибудь ещё
    if(alloc_region_addr==MAP_FAILED) return REGION_INVALID;
  }
  block_init(alloc_region_addr,(block_size){.bytes=actual},NULL);                                    // Всё хорошо, память размечена -> создаём в этом месте блок, причём передаем его размер, а не вместимость, т.к. пересчитается в функции
  return (struct region){.addr=alloc_region_addr,.size=actual,.extends=(alloc_region_addr == addr)}; // И ассоциируем его с регионом
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;
  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if(block_splittable(block,query)){
    void* new_block =  (block->contents)+query;                                             // Начинаем резать массив первого блока
    block_init(new_block,(block_size){.bytes=(block->capacity).bytes - query}, block->next);//|___заголовок1____|___________________данные1____________________| -> |___заголовок1____|_______данные1(урезанные)________||___заголовок2____|___данныe2___|, где второй блок занимает остаток пустых данных первого
    block->next = (struct block_header*)new_block;                                          // Перецепляем их между собой                                    
    block->capacity.bytes = query;                                                          // Урезаем вместимость первого блока до запрашиваемой
    return true;
  }
  return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block ){
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst!=NULL && snd!=NULL && fst->is_free && snd->is_free && blocks_continuous( fst, snd );
}

static bool try_merge_with_next( struct block_header* block ) {
    if(mergeable(block,block->next)){
      block->next->is_free=false;      // Второго блока больше не существует
      block->capacity.bytes += size_from_capacity(block->next->capacity).bytes; // Весь второй блок вливается в данные первого
      block->next = block->next->next; // Перецепляем через один: 1->2->3/NULL => 1->3/NULL
      return true;
    }
    return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  if(block==NULL) return (struct block_search_result){.type=BSR_CORRUPTED,.block=block}; //Чудо свершилось - в функцию попал NULL
  while(block!=NULL){
    while(try_merge_with_next(block));                   // До первого препятствия в виде занятого блока соединяем их (дальше мы перешагнём через это препятстсвие с помощью block->next)
    if(block->is_free && block_is_big_enough(sz,block)){ // Получилось собрать достаточно большой блок 
        //split_if_too_big(block,sz);                    // Можно улучшить использование памяти, но тогда не пройдёт тестер)
        return(struct block_search_result){.type=BSR_FOUND_GOOD_BLOCK,.block=block};
    }
    if(block->next!=NULL){
      block = block->next;
    }
    else{
      break;
    }
  }
  return (struct block_search_result){.type=BSR_REACHED_END_NOT_FOUND,.block=block}; //Нужный блок не найден -> возвращается последний
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  query = size_max(query,BLOCK_MIN_CAPACITY); //Валидация на минимум размера искомого блока
  struct block_search_result res = find_good_or_last(block,query);
  if(res.type==BSR_FOUND_GOOD_BLOCK){  // Нашли хороший блок
    split_if_too_big(res.block,query); // Урезаем его до оптимально нужного
    res.block->is_free=false;          // И занимаем его
  }
  return res;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if(last==NULL) return NULL;
  struct region added_region = alloc_region(block_after(last), query); //На конце списка аллоцируем регион
  struct block_header* added_block = added_region.addr;
  last->next = added_block;                                            //Связываем со следующим блоком
  //if(region_is_invalid(added_region.addr)) return NULL;              //Не получилось добавить памяти с конца
  if(last->is_free&&added_region.extends){                             //Последний блок кучи был свободен -> соединяем его с добавленным
    try_merge_with_next(last);                                         //Получилось соединить -> значит последний блок - это last(причём он подходит нам)
    return last;
  }
  else{
    return added_block;                                                // Не получилось соединить -> вернём только что добавленный
  }
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {      
  struct block_search_result res = try_memalloc_existing(query,heap_start); 
  if(res.type==BSR_REACHED_END_NOT_FOUND){
    grow_heap(res.block,query);                                             //res.block - содержит адрес последнего блока 
    res = try_memalloc_existing(query,heap_start);                          //Пробуем ещё раз, при этом мы уже можем присоединить
  }
  if (res.type==BSR_FOUND_GOOD_BLOCK)
  {
    return res.block;
  }
  return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while(try_merge_with_next(header)); //Соединяем все пустые блоки до первого занятого
}

#include "test_help.h"
int test_free(){
    void* heap = heap_init(HEAP_SIZE);
    if(heap==NULL) return 1;
    void* region1 = _malloc(REGION_SIZE);
    void* region2 = _malloc(REGION_SIZE);
    void* region3 = _malloc(REGION_SIZE);
    _free(region2);
    struct block_header* block1 = (struct block_header*) (region1- offsetof(struct block_header, contents));
    struct block_header* block2 = (struct block_header*) (region2- offsetof(struct block_header, contents));
    struct block_header* block3 = (struct block_header*) (region3- offsetof(struct block_header, contents));
    if(block1->capacity.bytes!=REGION_SIZE||block3->capacity.bytes!=REGION_SIZE){
        return 1;
    }
    if(block1->is_free||!(block2->is_free)||block3->is_free){
        return 1;
    }
    _free(region1);
    _free(region2);
    _free(region3);
    munmap(heap, size_from_capacity((block_capacity){.bytes = 3*REGION_SIZE}).bytes);
    return 0;
}

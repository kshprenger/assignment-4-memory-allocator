#include "test_help.h"
int test_good(){
    void* heap = heap_init(HEAP_SIZE);
    if(heap==NULL) return 1;
    void* region = _malloc(REGION_SIZE);
    struct block_header* block = heap;
    if(block->capacity.bytes!=REGION_SIZE || block->is_free==true){
        return 1;
    }
    _free(region);
    munmap(heap, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
    return 0;
}
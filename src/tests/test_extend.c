#include "test_help.h"
int test_extend(){
    void* heap = heap_init(HEAP_SIZE);
    if(heap==NULL) return 1;
    void* region1 = _malloc((HEAP_SIZE*3)/4);
    void* region2 = _malloc(REGION_SIZE);
    struct block_header* block1 = (struct block_header*) (region1- offsetof(struct block_header, contents));
    struct block_header* block2 = (struct block_header*) (region2- offsetof(struct block_header, contents));
    if(block1->capacity.bytes!=((HEAP_SIZE*3)/4)||block2->capacity.bytes!=REGION_SIZE||(struct block_header*)(block1->contents+block1->capacity.bytes)!=block2){
        return 1;
    }
    if(block1->is_free||block2->is_free){
        return 1;
    }
    _free(region1);
    _free(region2);
    munmap(heap, size_from_capacity((block_capacity){.bytes = (((HEAP_SIZE*3)/4)+REGION_SIZE)}).bytes); //Для седующего теста не затираем всю память
    return 0;
}

#include "tests/test.h"
#include <stdio.h>
int main(){
    int (*tests[5])();
    tests[0] = test_good;
    tests[1] = test_free;
    tests[2] = test_free2;
    tests[3] = test_extend;
    tests[4] = test_extend_out;
    for(size_t i = 0; i<5; i++){
        if(tests[i]()){
            printf("TEST %zu PASSED\n",i);
        }
        else{
            printf("TEST %zu FAILED\n",i);
        }
    }
    return 0;
}
